# The Quartermaster

This is a small project i stated to learn how to start using JavaScript, TypeScript, and how to interact with databases.😅
The plan is if to become a discord bot with an Ex-system, event polls, and other helpful or funny commands.

## Installation

### Requirements

The Quartermaster requires [Node.js][1] and [npm][2].

### Setup

Clone this repository or copy the The-Quartermaster directory to your machine.

Open a terminal in the The-Quartermaster directory and run the following command:

```shell
npm install
```

Create a file named *`config.json`* in the *`/src`* directory containing the following.
Replace `your-token-goes-here` with your own Discord bot's token.

```ts
export let token: string = "your-token-goes-here";

export let prefix: string = "!";

```

### Start up

Open a terminal in the The-Quartermaster directory and run the following command.
Closing the terminal window or killing the process will cause the bot to stop running.

```shell
node run go
```

## Contributing

Contributions are welcome! Feel free to fork the repository and submit a pull request. Do read our [guidelines][6] before submitting your changes thou. 🙂

## Updates

- v0.0.5-beta

## Attribution

The Quartermaster bot is being created by collaboration with [@LaytonGB][4]

Also, while this project is licensed under MIT, if you feel generous and wish to buy me and maintainers coffee, we'll happily accept.😄

| **Name**     | **Address**                                                                                        |
| ------------ | -------------------------------------------------------------------------------------------------- |
| Bitcoin      | `34YBHXNHC8HNdm7RzMEfuR6ViYZ5kPmrc8`                                                               |

[1]: https://nodejs.org/en/ "nodejs.org"
[2]: https://www.npmjs.com/ "npmjs.com"
[3]: https://gitlab.com/bbinaryg/the-quartermaster/-/issues/new "Create a new issue report"
[4]: https://gitlab.com/LaytonGB "LaytonGB"
[6]: https://gitlab.com/bbinaryg/the-quartermaster/-/blob/master/ "CONTRIBUTING.md"
