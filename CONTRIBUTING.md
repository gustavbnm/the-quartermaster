# We Ask You To Follow These Guidelines when Sending A Merge Request Or Issue Report

## Submitting Issues

Before creating an issue, please ensure that it hasn't already been reported/suggested, and double-check the [F.A.Q][5].

Copy & Paste the following template when submitting an issue.

Keep the titles short and to the point.

```json

`Title`: `< Issue Title >`

`Category`: `< Bug | Feature | Misc >`

`Description`:

    < Any information regarding the issue here. >
    < Be as detailed as possible. >

```

## Merge-Request Guidelines

Merge requests **not following** these guidelines will be **ignored**.

* Avoid overly-complex code.
    Follow the [KISS][1] guidelines.
* Ensure your code meets the project's current code-style.
* Test your changes personally before requesting them to be committed.
* Don't commit changes that simply refactor existing code or its code style.
* Avoid using code that can potential break on Discord updating.
* Comment each component of your changes using block quotes with the REVIEW heder and be as detailed as
    possible.

    ```ts

    // REVIEW
    /*

        ...

    */
    ```

* Ensure you follow ESLint's standards via the [configuration file](.eslintrc.json)
* Ensure your changes do not break do a test with. ( `npm run go` )

[1]: https://en.wikipedia.org/wiki/KISS_principle
[5]: #Contributing "The are currently no F.A.Q:s"
