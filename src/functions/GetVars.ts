export function getVarFromLabel(content: string, label: string): string | null {
    const expression = new RegExp(`((?<= ${label}= *)[\\s\\S][^ ]+)`, "g");
    const out = content.match(expression);
    if (out !== null) return out.toString();
    else return null;
}