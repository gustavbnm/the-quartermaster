import { Collection, Snowflake, Guild, GuildMember } from "discord.js";
import { AkairoClient } from "discord-akairo";
import { client, SQL } from "Bot";
import { prefix } from "Config";

/*
--- PLAN OUTLINE ---
create tables if needed
get the guilds, enter into guilds table
get users from guilds, enter into users table
get more user data, enter into user_guilds table, linking to users table and guilds table
*/

/// FIXME Is not woking is broke still tries to find guilds first
export const DB = {
    insertGuild: function (ref_id: string): void {
        SQL.prepare(`
            INSERT INTO guilds (
                ref_id, prefix, dj_role, default_dj_vol, max_level, default_timeout_dur
            ) VALUES (
                ?, '${prefix}', 'DJ', 50, 100, 300
            )
        `).run(ref_id);
    },
    getGuildRowId: function (ref_id: string): number {
        return SQL.prepare("SELECT id FROM guilds WHERE ref_id=?").pluck().get(ref_id);
    },
    getAllGuilds: async function (): Promise<Record<string, unknown>> {
        const guildIds: { ref_id: string }[] = SQL.prepare("SELECT ref_id FROM guilds").all();
        const guilds: Guild[] = [];
        for (const id of guildIds) {
            guilds.push(client.guilds.cache.get(id.ref_id));
        }
        function Guild(online: number, total: number) {
            this.online = online;
            this.total = total;
        }
        const out = {};
        const waitFor: Promise<void>[] = [];
        for (const guild of guilds) {
            const presences = guild.presences.cache;
            waitFor.push(
                guild.members.fetch({ withPresences: true })
                    .then(members => {
                        out[guild.name] = new Guild(
                            members.filter(member => {
                                try {
                                    return presences.get(member.id).status !== "offline";
                                } catch {
                                    return false;
                                }
                            }).size,
                            members.size,
                        );
                    })
                    .catch(console.error)
            );
        }
        await Promise.all(waitFor);
        return out;
    },
    insertUser: function (ref_id: string): void {
        SQL.prepare(`
            INSERT INTO users (
                ref_id, tz_pref
            ) VALUES (
                ?, ''
            )
        `).run(ref_id);
    },
    getUserRowId: function (ref_id: string): number {
        return SQL.prepare("SELECT id FROM users WHERE ref_id=?").pluck().get(ref_id);
    },
    insertUserGuild: function (user_id: number, guild_id: number): void {
        SQL.prepare(`
            INSERT INTO user_guilds (
                user_id, guild_id, level, xp, timeout_end
            ) VALUES (
                ?, ?, 0, 0, 0
            )
        `).run(user_id, guild_id);
    },
    getUserGuildRowId: function (user_id: number, guild_id: number): number {
        return SQL.prepare("SELECT id FROM user_guilds WHERE user_id=? AND guild_id=?").pluck().get(user_id, guild_id);
    },
};

export function makeDBs(thisClient: AkairoClient): void {

    /* ------ CREATE TABLES AS NEEDED ----- */
    SQL.prepare(`CREATE TABLE IF NOT EXISTS guilds (
        id INTEGER PRIMARY KEY,
        ref_id VARCHAR UNIQUE NOT NULL,
        prefix VARCHAR,
        dj_role VARCHAR,
        default_dj_vol INTEGER,
        max_level INTEGER,
        default_timeout_dur INTEGER
    )`).run();
    SQL.prepare(`CREATE TABLE IF NOT EXISTS users (
        id INTEGER PRIMARY KEY,
        ref_id VARCHAR UNIQUE NOT NULL,
        tz_pref VARCHAR,
        guild_id INTEGER,
        FOREIGN KEY(guild_id) REFERENCES guilds(id)
    )`).run();
    SQL.prepare(`CREATE TABLE IF NOT EXISTS user_guilds (
        id INTEGER PRIMARY KEY,
        user_id INTEGER NOT NULL,
        guild_id INTEGER NOT NULL,
        level INTEGER,
        xp INTEGER,
        timeout_end INTEGER,
        FOREIGN KEY(user_id) REFERENCES users(id),
        FOREIGN KEY(guild_id) REFERENCES guilds(id),
        UNIQUE(user_id, guild_id)
    )`).run();


    /* ----- PREPARE INSERT AND GET STATEMENTS ----- */
    const insertManyUsers = SQL.transaction(users => {
        /* Receives a Set of user IDs in the discord number string format. */
        for (const user of users) {
            DB.insertUser(user);
        }
    });
    const insertManyUserGuilds = SQL.transaction(users => {
        for (const user of users) {
            if (!DB.getUserGuildRowId(user[0], user[1])) DB.insertUserGuild(user[0], user[1]);
        }
    });

    const USERS: Set<string> = new Set(); // users to be inserted to sql table "users"
    const GUILDUSERS: [string | number, string | number][] = []; // user's accounts on guilds to be inserted to sql table "user_guilds"
    const guildUsers: [string | number, Promise<Collection<Snowflake, GuildMember>>][] = []; // contains guild users-promise tuple

    /* ----- ADD GUILDS AND USERS AS NEEDED ----- */
    thisClient.guilds.cache.forEach((guild) => {

        /* - ADD GUILDS - */
        let guildRowId = DB.getGuildRowId(guild.id);
        if (!guildRowId) {
            DB.insertGuild(guild.id);
            guildRowId = DB.getGuildRowId(guild.id);
        }
        guildUsers.push([ guildRowId, guild.members.fetch() ]);
    });

    const readyPromises = [];
    for (let i = 0; i < guildUsers.length; i++) {
        readyPromises.push(
            guildUsers[i][1].then(users => {
                users.forEach((user, key: string) => {
                    GUILDUSERS.push([key, guildUsers[i][0]]);
                    if (!DB.getUserRowId(key)) { // we don't want to include users that are already listed
                        USERS.add(key);
                    }
                });
            }).catch(console.error)
        );
    }

    Promise.all(readyPromises).then(() => {
        insertManyUsers(USERS);
    }, console.error).catch(console.error)
        .then(() => {
            GUILDUSERS.forEach(user => user[0] = DB.getUserRowId(user[0] as string));
            insertManyUserGuilds(GUILDUSERS);
        }, console.error).catch(console.error);
}

/* --- INSERT COLLECTED USERS INTO SQL TABLE "USERS" --- */
// users_arr.forEach((users, i) => {
//     users.forEach((user, key) => {
//         sql_users.push({
//             ref_id: key,
//             tz_prefs: "UTC",
//             guild_id: guild_arr[i],
//         });
//     });
// });
// console.log(sql_users);
// // insert_user.run(sql_users);

/*
const list = client.guilds.cache.get(guild.id);
    list.members.cache.forEach(member => {
        //do stuff with guild members here
    }
*/

/*
// First we use guild.members.fetch to make sure all members are cached
<guild>.members.fetch().then(fetchedMembers => {
	const totalOnline = fetchedMembers.filter(member => member.presence.status === 'online');
	// We now have a collection with all online member objects in the totalOnline variable
	console.log(`There are currently ${totalOnline.size} members online in this guild!`)
});
*/

/*
this.client.guilds.members.fetch().then(fetchedMembers => {
    const totalOnline = fetchedMembers.filter(member => member.presence.status === 'online');
    msg.channel.send(`There are currently ${totalOnline.size} members online in ${msg.guild.name}!`);
});
*/
/* client.on("ready", () => {
    // Check if the table "points" exists.
    const table = sql.prepare("SELECT count(*) FROM sqlite_master WHERE type='table' AND name = 'scores';").get();
    if (!table['count(*)']) {
      // If the table isn't there, create it and setup the database correctly.
        sql.prepare("CREATE TABLE scores (id TEXT PRIMARY KEY, user TEXT, guild TEXT, points INTEGER, level INTEGER);").run();
      // Ensure that the "id" row is always unique and indexed.
        sql.prepare("CREATE UNIQUE INDEX idx_scores_id ON scores (id);").run();
        sql.pragma("synchronous = 1");
        sql.pragma("journal_mode = wal");
    }

    // And then we have two prepared statements to get and set the score data.
    client.getScore = sql.prepare("SELECT * FROM scores WHERE user = ? AND guild = ?");
    client.setScore = sql.prepare("INSERT OR REPLACE INTO scores (id, user, guild, points, level) VALUES (@id, @user, @guild, @points, @level);");
}); */