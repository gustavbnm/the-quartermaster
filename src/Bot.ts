import { token, owners } from "./Config";
import BotClient from "./client/BotClient";
import Database = require("better-sqlite3");

export const client: BotClient = new BotClient({token, owners});
export const SQL = new Database("./src/database.db"); //  { verbose: console.log }

client.start();

//SECTION
// ANCHOR - Used to indicate a section in your file
// TODO - An item that is awaiting completion
// FIXME - An item that requires a bugfix
// STUB - Used for generated default snippets
// NOTE - An important note for a specific code section
// REVIEW - An item that requires additional review
// SECTION - Used to define a region (See 'Hierarchical anchors')
// LINK - Used to link to a file that can be opened within the editor (See 'Link Anchors')
//!SECTION ----