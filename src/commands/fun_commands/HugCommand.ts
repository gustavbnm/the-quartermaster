import { MyCommand } from "@client";
import { Message, Role  } from "discord.js";
import { prefix } from "Config";
import * as fs from "fs";

export default class hug extends MyCommand {
    public constructor() {
        super("Hug", {
            aliases: ["hug", "embrace"],
            category:"Fun Commands",
            channel: "guild",
            description: {
                content:"This command tell you how you hug the mentioned user.",
                snippet:"Hug someone",
                usage: "To hug someone.",
                examples: [
                    `${prefix}hug @The Quartermaster`
                ],
            },
            ratelimit: 3 //NOTE number of times you can spam this sucker
        });
    }

    public exec(msg: Message): Promise<void|Message> {
        fs.readFile("src/commands/fun_commands/hugs.txt", (err, tex: Buffer) => {
            if (err) throw err;
            const hugs = tex.toString().split(/\r?\n/);
            const hug = hugs[Math.floor((Math.random() * hugs.length) + 1)];
            msg.delete();
            let mentioned: string | Role;
            if (msg.mentions.users.size) {
                mentioned = `<@${msg.mentions.users.first().id}>`;
            } else {
                return msg.channel.send(`Something went wrong do \`${prefix}hug\` to see how to use this command..`);
            }
            msg.channel.send(`${msg.author} gives ${mentioned} ${hug}`);
        });
        return;
    }
}
