import { Message, MessageEmbed, MessageMentions, Role } from "discord.js";
import { client } from "Bot";
import { prefix } from "Config";
import { MyCommand } from "@client";
import { getUserFromMessage } from "@functions/GetGuildUserData";
import { emoji } from "@functions/FindUserEmoji";
import { stripIndents } from "common-tags";

export default class CommandName extends MyCommand {
    public constructor() {
        super("name", {
            aliases: ["name", "name2"],
            category:"Undefined",
            channel: "guild",
            description: {
                content:"Super long and detailed long description",
                snippet:"Short description",
                usage: `${prefix}cookies`,
                examples: [
                    `${prefix}cookies`,
                    `${prefix}cookies give @user`
                ]
            },
            args: [
                {
                    id: "member",
                    type: "userMention",
                    default: null
                },
                {
                    id: "give",
                    default: null
                }
            ],
            ratelimit: 3  //NOTE number of times you can spam this sucker
        });
    }

    public exec( msg : Message, args: { member: { users: { size: unknown; }; length: unknown; }; phrase: string; give: string; } ): Promise < void | Message > {

        let role: Role;
        const user = getUserFromMessage(msg);
        if (msg.mentions.roles.size > 0) role = msg.mentions.roles.first();

        if(args.member.users.size == 1 && args.phrase == "give"){

            // TODO Take from one user's Cookie db and give to another

            return;
        }
        else if ((args.member.length !== 1 && args.give == "give") || args.member.length > 1){
            return msg.channel.send(`Something went wrong do \`${prefix}help cookies\` to see how to use this command..`);
        }
        else if (args.member.length  == 1 && args.give !== "give"){
            return msg.channel.send(new MessageEmbed()
                .setColor(role.hexColor)
                .setTitle("COOKIES!")
                .setDescription(stripIndents`

                `)                  // TODO Get Cookies Stats form DB of role = msg.mentions;
                .setFooter(`Requested by @${user.user.tag} at`, user.user.displayAvatarURL({ dynamic: true }))
                .setTimestamp()

            );
        }

    }

}
