import { MyCommand } from "@client";
import { Message, MessageEmbed } from "discord.js";
import { prefix } from "Config";
import { getUserFromMessage } from "@functions/GetGuildUserData";

export default class PingCommand extends MyCommand {
    public constructor() {
        super("Ping", {
            aliases: ["Ping"],
            category:"Core Commands",
            description: {
                content:"Check the latency of the ping to the Discord API",
                snippet:"Pings the server.",
                usage: `${prefix}ping`,
                examples: [
                    `${prefix}ping`
                ],
            },
            ratelimit: 3 //NOTE number of times you can spam this sucker

        });
    }

    public exec( message : Message): Promise < void | Message > {
        return message.util.send("🏓 Pinging....").then((msg: Message) => {
            const user = getUserFromMessage(msg);
            // const msgAvatar = user.user.displayAvatarURL({ dynamic: true });
            let msgColor = user.displayHexColor;
            if (msgColor == "#000000") msgColor = "RANDOM";
            const msgOut = new MessageEmbed()
                .setTitle("Pong!")
                .setDescription(
                    `🏓 Pong!
                    \nLatency is \`${Math.floor(msg.createdTimestamp - message.createdTimestamp)}\` ms
                    \nAPI Latency is \`${Math.round(this.client.ws.ping)}\` ms`
                )
                .setColor(msgColor);
            msg.edit(msgOut);
        }).catch((reason: string) => {
            message.util.send(`Whoops, something went wrong!\nReason given: ${reason}`);
        });
    }
}
