import { Message, MessageEmbed } from "discord.js";
import { prefix } from "Config";
import { MyCommand } from "@client";
import { stripIndents } from "common-tags";
import { getUserFromMessage } from "@functions/GetGuildUserData";
export default class CommandName extends MyCommand {
    public constructor() {
        super("Help", {
            aliases: ["help", "commands", "cmds"],
            category:"Core Commands",
            channel: "guild",
            description: {
                content:"Shows all available bot commands and how to use them.",
                snippet:"...",
                usage: `${prefix}help [command]`,
                examples: [
                    `${prefix}help`,
                    `${prefix}help event`
                ]
            },
            ratelimit: 3,
            args: [
                {
                    id: "command",
                    type: "commandAlias",
                    default: null
                }
            ]
        });
    }

    public exec(msg: Message, { command }: {command: MyCommand} ): Promise < void | Message > {

        const user = getUserFromMessage(msg);
        let msgColor = user.displayHexColor;
        if (msgColor == "#000000") msgColor = "RANDOM";

        if(command) {
            return msg.channel.send(new MessageEmbed()
                .setColor(msgColor)
                .setTitle(`${prefix}${command}`)
                .setDescription(stripIndents`
                    **Description:**
                    ${command.description.content || "No content provided."}

                    **Usage:**
                    \`${command.description.usage || "No usage provided."}\`

                    **Examples:**
                    ${command.description.examples ? command.description.examples.map(e => `\`${e}\``).join("\n") : "No examples provided."}
                `)
                .setFooter(`Requested by @${user.user.tag} at`, user.user.displayAvatarURL({ dynamic: true }))
                .setTimestamp()
            );
        }
        // const msgAvatar = user.user.displayAvatarURL({ dynamic: true });

        const theEmbed = new MessageEmbed() // TODO make prettier Embed
            .setColor(msgColor)
            .setTitle("Available commands")
            .setDescription(`If you want more information regarding a specific command, use \`${prefix}help [command]\` replacing command with the command you want xD`)
            .setFooter(`Requested by @${user.user.tag}`, user.user.displayAvatarURL({ dynamic: true }))
            .setTimestamp();

        for (const category of this.handler.categories.values()) { // TODO If the sum of all characters in an embed exceed 6000 characters split into help 1 and help 2
            if (["default"].includes(category.id)) continue;

            theEmbed.addField(
                category.id,
                category
                    .filter(cmd => cmd.aliases.length > 0)
                    .map(cmd => `\`${prefix}${cmd.toString().toLowerCase()}\` - ${cmd.description.snippet || "This command lacks description."}`)
                    .join("\n") || "No commands in this category.",
                true  // NOTE <----FUCK little shit!
            );
        }
        return msg.channel.send(theEmbed);
    }
}
