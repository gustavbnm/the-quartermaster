import { Listener } from "discord-akairo";
import { prefix } from "Config";
import { makeDBs, DB } from "@db";
import { RunOnGO } from "RunOnGo";
export default class ReadyListener extends Listener {
    public constructor() {
        super("ready", {
            emitter: "client",
            event:"ready",
            category:"client"
        });
    }

    // Thus runs when the Bot is ready
    public exec(): void {
        this.client.user.setActivity(`the horizon | ${prefix}help`, { type: "WATCHING" });
        console.log(`\n${this.client.user.username} in onboard the ships: `);

        makeDBs(this.client);

        DB.getAllGuilds().then(guilds => {
            if (guilds) console.table(guilds);
            else console.error(" > No guilds were found. Did the database fail to create?");
        });

        RunOnGO(true);
    }
}
